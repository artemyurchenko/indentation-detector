import IndentationType.*
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

const val usageMessage = "Usage: [PATH-TO-JAVA-SOURCE]"

fun main(args: Array<String>) {
    if (args.size != 1) {
        System.err.println("Invalid usage: $usageMessage")
        return
    }
    try {
        val (indentSymbol, indentSize) = getIndentationDetails(args[0])
        when (indentSymbol) {
            NONE -> {
                println("${SPACE.name} 0")
                System.err.println("Not enough data to determine indentation type, defaulting to spaces")
            }
            MIXED -> {
                println("${SPACE.name} 0")
                System.err.println("Indentation on every line consists of both spaces and tabs, defaulting to spaces")
            }
            SPACE, TAB -> println("${indentSymbol.name} $indentSize")
        }
    } catch (e: IOException) {
        System.err.println("Can't open the file: ${args[0]}")
    }
}

fun getIndentationDetails(filename: String): Pair<IndentationType, Int> {
    val lines = Files.lines(Paths.get(filename))
    val detector = IndentationDetector()
    lines.forEach { detector.update(it) }
    return detector.getIndentationDetails()
}
