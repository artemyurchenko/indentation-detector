import static groovy.io.FileType.FILES

class MainKtTest extends GroovyTestCase {
    void testGetIndentationDetails() {
        new File("./tests/").eachFile(FILES) {
            if (it.name.endsWith(".test")) {
                def expectedName, expectedSize
                it.withReader { (expectedName, expectedSize) = it.readLine().split() }
                expectedSize = expectedSize.toInteger()
                def actualAnswer = MainKt.getIndentationDetails(it.absolutePath)
                def actualName = actualAnswer.first.name()
                def actualSize = actualAnswer.second
                assertEquals("Incorrect indentation symbol", expectedName, actualName)
                assertEquals("Incorrect indentation size", expectedSize, actualSize)
            }
        }

    }
}
