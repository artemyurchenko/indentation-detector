import IndentationType.*

class IndentationDetector {
    private var lastIndentType = NONE
    private var lastIndentNumber = 0
    private var linesCount = 0

    private val spaceDiffs = hashMapOf<Int, Int>()
    private val tabDiffs = hashMapOf<Int, Int>()


    fun update(line: String) {
        ++linesCount

        val (indentType, indentNumber) = getIndentationInfo(line)

        if (indentType == IndentationType.MIXED) return

        val sizeDiff = Math.abs(lastIndentNumber - indentNumber)
        if (sizeDiff != 0) {
            if (indentType == NONE) {
                spaceDiffs.merge(sizeDiff, 1, Int::plus)
                tabDiffs.merge(sizeDiff, 1, Int::plus)
            } else if (lastIndentType == NONE || lastIndentType == indentType) {
                when (indentType) {
                    SPACE -> spaceDiffs.merge(sizeDiff, 1, Int::plus)
                    else -> tabDiffs.merge(sizeDiff, 1, Int::plus)
                }
            }
        }
        lastIndentType = indentType
        lastIndentNumber = indentNumber
    }

    fun getIndentationDetails(): Pair<IndentationType, Int> {
        if (linesCount == 0) {
            return Pair(NONE, 0)
        }
        if (spaceDiffs.isEmpty() && tabDiffs.isEmpty()) {
            return Pair(MIXED, 0)
        }
        val spaceMaxDiff = spaceDiffs.maxBy { it.value }?.toPair() ?: Pair(0, -1)
        val tabMaxDiff = tabDiffs.maxBy { it.value }?.toPair() ?: Pair(0, -1)
        if (spaceMaxDiff.second >= tabMaxDiff.second) {
            return Pair(SPACE, spaceMaxDiff.first)
        } else {
            return Pair(TAB, tabMaxDiff.first)
        }
    }

    companion object {
        private val spaceRegex = "^( +)[^\t].*".toRegex()
        private val tabRegex = "^(\t+)[^ ].*".toRegex()

        private fun getIndentationInfo(line: String): Pair<IndentationType, Int> {
            val spaceMatcher = spaceRegex.find(line)
            if (spaceMatcher != null) {
                return Pair(SPACE, spaceMatcher.groupValues[1].length)
            }
            val tabMatcher = tabRegex.find(line)
            if (tabMatcher != null) {
                return Pair(TAB, tabMatcher.groupValues[1].length)
            }
            if (line.startsWith(' ') || line.startsWith('\t')) {
                return Pair(MIXED, 0)
            }
            return Pair(NONE, 0)
        }
    }
}

enum class IndentationType { NONE, MIXED, SPACE, TAB; }
